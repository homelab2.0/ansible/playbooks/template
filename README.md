# Ansible Playbook: Template

<!-- [![CI](https://github.com/geerlingguy/ansible-role-docker/workflows/CI/badge.svg?event=push)](https://github.com/geerlingguy/ansible-role-docker/actions?query=workflow%3ACI) -->

An Ansible Playbook Template To Base Future Playbooks On.

## Requirements

- None

## Required Variables

- None

## Dependencies

[Sleuth56's Template Role](https://gitlab.com/homelab2.0/ansible/roles/template)

## Run

1. Install Ansible
2. Copy and run the folling two lines.
```bash
ansible-galaxy install -r requirements.yml --force
ansible-playbook playbook.yml -i inventories/main
```

## License

GPLv3

## Author Information

This playbook was created in 2024 by Stephan Burns.